#!/bin/bash
clear
read -p "Please enter hour: " hour
read -p "Please enter minute: " minute
date_hour=`date +%H`
date_minute=`date +%M`
sigma_date=$((date_hour * 60 + date_minute))
sigma_date_input=$((hour * 60 + minute))
while [ "$sigma_date" != "$sigma_date_input" ]
do
    sleep 20
    date_hour=`date +%H`
    date_minute=`date +%M`
    sigma_date=$((date_hour * 60 + date_minute))
done

cd $HOME/Downloads/tonight_hunt
split -l 1 downloads.txt
mv downloads.txt ..
download_links=`ls`
for i in $download_links
do
    download_candidate=`cat $i`
    wget $download_candidate
done

rm x*
init 0

# in newer version, add this feauturs:
    # check if hour == 8 (or user input) then stop downloading
    # be smarter than this beta version
    # control download status and if it working bad, check the solution and if possible, fix it

